#Инициализация основного докера
FROM golang:alpine
LABEL maintainer="mr.wgw@yandex.ru"
# Copy configuration and source code of server
RUN rm -rf ./server
RUN mkdir -p ./server/bin
# WORKDIR /server/bin
ADD ./documents/config.json ./server/bin
# WORKDIR /server
ADD ./ ./server
# Build Server
WORKDIR ./server/src
# Preparing Go packages
RUN go env -w GO111MODULE=on
RUN go get github.com/gorilla/mux
RUN go get golang.org/x/net/http2
RUN go get github.com/jackc/pgx
RUN go get github.com/jackc/pgx/v4/pgxpool
RUN go get github.com/valyala/fasthttp
RUN go build -o ./timetabler ./main.go
RUN echo $(ls -1 ./)
RUN mv timetabler ../bin/timetabler
# Start Server
WORKDIR ../bin
ENTRYPOINT ["./timetabler"]
