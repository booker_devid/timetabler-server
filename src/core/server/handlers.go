package server

import (
	"fmt"
	"net/http"

	"src/data/database"
	"src/utility"
)

var ver = "0.0.1"

//Handlers - router for server
func (root *Core) Handlers(errCh chan<- string) {
	//Check server
	root.proc.router.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {
		http.SetCookie(w, &http.Cookie{
			Name:   "Test",
			Value:  "Cookie",
			MaxAge: 1000,
		})
		w.Write([]byte(fmt.Sprintf("TimeTabler server %s", ver)))
	}).Methods("GET")
	//Авторизация пользователя в базе данных
	root.proc.router.HandleFunc("/api/auth", func(w http.ResponseWriter, r *http.Request) { //READY
		web := r.URL.Query().Get("web")
		login, pwd, ok := r.BasicAuth()
		if web != "t" {
			web = ""
		}
		fmt.Println(login, pwd)
		if ok {
			data := database.Authorize(root.db, &login, &pwd, &web, &r.RemoteAddr) //data, enscryption_session, err
			if data.Error != nil {
				errCh <- data.GetError()
				w.Header().Add("Err", data.Key)
				switch data.Key {
				case utility.KEYS[1], utility.KEYS[2], utility.KEYS[4]:
					{
						w.WriteHeader(401)
						return
					}
				case utility.KEYS[3]:
					{
						w.WriteHeader(420)
						return
					}
				}
			} else {
				w.Header().Set("Content-Type", "application/json")
				if data.EncryptionSession != "" {
					http.SetCookie(w, &http.Cookie{
						Name:   "user-s-data",
						Value:  data.EncryptionSession,
						MaxAge: 2592000,
						// Secure: true,
					})
				}
				w.Write(data.Data)
				return
			}
		} else {
			w.Header().Add("Err", "Invalid basic or parameters")
			w.WriteHeader(401)
			return
		}
	}).Methods("PUT")
	//Проверка сессии на наличие сookie c токеном сессии
	root.proc.router.HandleFunc("/api/let-me-in", func(w http.ResponseWriter, r *http.Request) { //READY
		var session string
		for _, cki := range r.Cookies() {
			if cki.Name == "user-s-data" {
				session = cki.Value
			}
		}
		w.Write([]byte(session))
		// w.WriteHeader(200)
		// if len(session) > 1 {
		// 	data, err := database.CheckSession(root.db, &session, &r.RemoteAddr)
		// 	if err != nil {
		// 		errCh <- err.Error()
		// 		w.Header().Add(http.CanonicalHeaderKey("Err"), err.Error())
		// 		w.WriteHeader(401)
		// 	} else {
		// 		w.Header().Set("Content-Type", "application/json")
		// 		w.Write(data)
		// 	}
		// } else {
		// 	w.Header().Add(http.CanonicalHeaderKey("Err"), "Invalid basic")
		// 	w.WriteHeader(401)
		// }
	}).Methods("GET")
	//Проверка существующего токена сессии на действительность
	// root.proc.router.HandleFunc("/api/let-me-in", func(w http.ResponseWriter, r *http.Request) { //READY
	// 	session := r.URL.Query().Get("session")
	// 	if len(session) > 1 {
	// 		data, err := database.CheckSession(root.db, &session, &r.RemoteAddr)
	// 		if err != nil {
	// 			errCh <- err.Error()
	// 			w.Header().Add(http.CanonicalHeaderKey("Err"), err.Error())
	// 			w.WriteHeader(401)
	// 		} else {
	// 			w.Header().Set("Content-Type", "application/json")
	// 			w.Write(data)
	// 		}
	// 	} else {
	// 		w.Header().Add(http.CanonicalHeaderKey("Err"), "Invalid basic")
	// 		w.WriteHeader(401)
	// 	}
	// 	return
	// }).Methods("PUT")
	//Верификация токена
	// root.proc.router.HandleFunc("/api/check", func(w http.ResponseWriter, r *http.Request) { //READY
	// 	token := r.URL.Query().Get("token")
	// 	phone := r.URL.Query().Get("phone")
	// 	if len(token) == 128 {
	// 		_, err := database.CheckToken(root.db, token)
	// 		if err != nil {
	// 			errCh <- err.Error()
	// 			w.Header().Add(http.CanonicalHeaderKey("Err"), err.Error())
	// 			w.WriteHeader(401)
	// 		} else {
	// 			w.WriteHeader(200)
	// 		}
	// 	} else if len(phone) > 10 && len(phone) < 20 {
	// 		err := database.CheckPhone(root.db, phone)
	// 		if err != nil {
	// 			errCh <- err.Error()
	// 			w.Header().Add(http.CanonicalHeaderKey("Err"), err.Error())
	// 			w.WriteHeader(401)
	// 		} else {
	// 			w.WriteHeader(200)
	// 		}
	// 	} else {
	// 		w.Header().Add(http.CanonicalHeaderKey("Err"), "Empty parameters")
	// 		w.WriteHeader(401)
	// 	}
	// }).Methods("GET")
	// //Регистрация пользователя в базе данных
	// root.proc.router.HandleFunc("/api/reg", func(w http.ResponseWriter, r *http.Request) { //READY
	// 	web := r.URL.Query().Get("web")
	// 	if web != "t" {
	// 		web = ""
	// 	}
	// 	data, enscryption_session, err := database.RegisterUser(root.db, r, &web)
	// 	if err != nil {
	// 		errCh <- err.Error()
	// 		w.Header().Add(http.CanonicalHeaderKey("Err"), err.Error())
	// 		w.WriteHeader(401)
	// 	} else {
	// 		w.Header().Set("Content-Type", "application/json")
	// 		if enscryption_session != "" {
	// 			http.SetCookie(w, &http.Cookie{
	// 				Name:   "session",
	// 				Value:  enscryption_session,
	// 				MaxAge: 1,
	// 			})
	// 		}
	// 		w.Write(data)
	// 	}
	// }).Methods("PUT")
	// //Выгрузка пользователей по фильтру
	// root.proc.router.HandleFunc("/api/users", func(w http.ResponseWriter, r *http.Request) { //READY
	// 	token := r.URL.Query().Get("token")
	// 	filter := r.URL.Query().Get("filter")
	// 	if len(token) == 128 {
	// 		data, err := database.GetListUsers(root.db, token, filter)
	// 		if err != nil {
	// 			errCh <- err.Error()
	// 			w.Header().Add("Err", err.Error())
	// 			w.WriteHeader(401)
	// 		} else {
	// 			w.Write(data)
	// 		}
	// 	} else {
	// 		w.Header().Add("Err", "Empty parameters")
	// 		w.WriteHeader(401)
	// 	}
	// }).Methods("GET")
}
