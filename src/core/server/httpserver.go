package server

import (
	"bufio"
	"context"
	"log"
	"net"
	"net/http"
	"os"

	"src/utility"

	"github.com/gorilla/mux"
	"golang.org/x/net/http2"
)

var errorHTTP = "Http process error# "

//HTTPProcess - struct server
type HTTPProcess struct {
	router *mux.Router
	server *http.Server
}

//InitServer - init http server
func InitServer() *HTTPProcess {
	rout := mux.NewRouter()
	file, _ := os.Create("web.log")
	writeInFile := bufio.NewWriter(file)
	server := &http.Server{
		ErrorLog: log.New(writeInFile, "fulldriver-server: ", 0),
		Handler:  rout,
	}
	return &HTTPProcess{rout, server}
}

//StartProcess starting http server
func (root *HTTPProcess) StartProcess(conf *utility.Configuration) error {
	http2Server := http2.Server{}
	err := http2.ConfigureServer(root.server, &http2Server)
	if err != nil {
		return utility.ErrorHandler(errorHTTP+" StartProcess.ConfigureServer ", err)
	}
	listen, err := net.Listen("tcp", conf.AddressServ)
	if err != nil {
		return utility.ErrorHandler(errorHTTP+" StartProcess.net.Listen ", err)
	}
	if err := root.server.Serve(listen); err != nil {
		return utility.ErrorHandler(errorHTTP+" StartProcess.ServeTLS ", err)
	}
	return nil
}

//RestartProcess - function restart proccess fasthttp server
func (root *HTTPProcess) RestartProcess(conf *utility.Configuration) error {
	if err := root.Shutdown(context.Background()); err != nil {
		return err
	}
	if err := root.StartProcess(conf); err != nil {
		return err
	}
	return nil
}

//Shutdown stopping http server
func (root *HTTPProcess) Shutdown(ctx context.Context) error {
	if err := root.server.Shutdown(ctx); err != nil {
		return utility.ErrorHandler(errorHTTP, err)
	}
	return nil
}
