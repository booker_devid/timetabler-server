package database

import (
	"context"

	"src/data/models"
	"src/utility"
)

//GetListChapters - выгрузка списка глав курсов из БД
func (db *Database) GetListChapters(id int) (models.ModelListChapter, error) {
	var chapters models.ModelListChapter
	var chapter models.ModelChapter
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return chapters, utility.ErrorCreate("GET CHAPTERS", err.Error())
	}
	data, err := connToDatabase.Query(context.Background(), "SELECT id, number_part, name_part, description_part FROM chapter WHERE course_id = $1", id)
	if err != nil {
		return chapters, utility.ErrorCreate("GET CHAPTERS", err.Error())
	}
	for data.Next() {
		data.Scan(&chapter.ID, &chapter.Number, &chapter.Title, &chapter.Description)
		chapters.List = append(chapters.List, chapter)
	}
	return chapters, nil
}
