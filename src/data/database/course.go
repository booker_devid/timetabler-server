package database

import (
	"context"
	"time"

	"src/data/models"
	"src/utility"

	pgx "github.com/jackc/pgx/v4"
)

//GetListCources - выгрузка списка курсов по фильтру
func (db *Database) GetListCources(filter string, id int) (models.ModelListCources, error) {
	var courses models.ModelListCources
	var course models.ModelCource
	var data pgx.Rows
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return courses, utility.ErrorCreate("GET COURCES", err.Error())
	}
	switch filter {
	case "person":
		data, err = connToDatabase.Query(context.Background(), "SELECT course.id, course_name, author_id, description_course, date_create FROM (SELECT course_id FROM cons_student_course WHERE student_id = $1) AS cr, course WHERE cr.course_id = course.id", id)
	default:
		data, err = connToDatabase.Query(context.Background(), "SELECT id, course_name, author_id, description_course, date_create FROM course")
	}
	if err != nil {
		return courses, utility.ErrorCreate("GET COURCES", err.Error())
	}
	for data.Next() {
		var date time.Time
		data.Scan(&course.ID, &course.Title, &course.AuthorID, &course.Description, &date)
		course.DateCreate = date.Format("yyyy-MM-dd")
		courses.List = append(courses.List, course)
	}
	return courses, nil
}
