package database

import (
	"context"
	"fmt"

	"src/utility"
)

//Проверка на существование
// _, err := db.AuthUserCheckLoginInDatabase(phone)
// if err != nil {
// 	return false, err
// }

//RegUserInDatabase - Функция для регстирации пользователя.
func (db *Database) RegUserInDatabase(
	firstname string, lastname string, middlename string, birthday string,
	phone string, emailaddres string, pwd string) (bool, error) {
	//Регистрация пользователя
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return false, utility.ErrorCreate("REG", err.Error())
	}
	//Начало транзакции
	transaction, err := connToDatabase.Begin(context.Background())
	defer transaction.Rollback(context.Background())
	//Подготовка телефона
	phone = utility.AdaptivePhone(phone)
	//Пишем в бд
	var userID int
	fmt.Println(firstname, lastname, middlename, birthday, phone, emailaddres, pwd)
	if err := transaction.QueryRow(
		context.Background(),
		"INSERT INTO users (phone, firstname, secondname, middlename, birthday, pwd) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id",
		phone, firstname, lastname, middlename, birthday, pwd).Scan(&userID); err != nil {
		return false, utility.ErrorCreate("REG", err.Error())
	}
	if _, err := transaction.Exec(
		context.Background(),
		"INSERT INTO students (id) VALUES ($1)", userID); err != nil {
		return false, utility.ErrorCreate("REG", err.Error())
	}
	transaction.Commit(context.Background())
	return true, nil
}
