package database

import (
	"context"
	"src/data/models"
	"src/utility"
	"time"

	pgx "github.com/jackc/pgx/v4"
)

//GetListUsers - выгрузка списка курсов по фильтру
func (db *Database) GetListUsers(filter string) (models.ModelListUser, error) {
	var users models.ModelListUser
	var user models.ModelUser
	var data pgx.Rows
	//Take connect form database
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	defer connToDatabase.Release()
	if err != nil {
		return users, utility.ErrorCreate("GET USER", err.Error())
	}
	//Use some case for choose a filter load users from database
	switch filter {
	case "teacher":
		{
			//TODO: change query for teachers
			data, err = connToDatabase.Query(context.Background(), "SELECT id, firstname, secondname, middlename, birthday, mail, phone FROM users")
		}
	case "student":
		{
			//TODO: change query for student
			data, err = connToDatabase.Query(context.Background(), "SELECT id, firstname, secondname, middlename, birthday, mail, phone FROM users")
		}
	case "enrollee":
		{
			//TODO: change query for enrolle
			data, err = connToDatabase.Query(context.Background(), "SELECT id, firstname, secondname, middlename, birthday, mail, phone FROM users")
		}
	default:
		{
			//Default case is load all users
			data, err = connToDatabase.Query(context.Background(), "SELECT id, firstname, secondname, middlename, birthday, mail, phone FROM users")
		}
	}
	if err != nil {
		return users, utility.ErrorCreate("GET USER", err.Error())
	}
	for data.Next() {
		var date time.Time
		data.Scan(&user.ID, &user.Firstname, &user.Secondname, &user.Middlename, &user.Birthday, &user.Email, &user.Phone, &user.Status)
		user.Birthday = date.Format("yyyy-MM-dd")
		users.User = append(users.User, user)
	}
	return users, nil
}

//SetStatusUserInDatabase - задать статус пользователя
func (db *Database) SetStatusUserInDatabase(id string, status string) error {
	connToDatabase, err := db.dbConnectPool.Acquire(context.Background())
	if err != nil {
		return utility.ErrorCreate("SET USER STATUS TAKE DBCON", err.Error())
	}
	defer connToDatabase.Release()
	if status == "admin" || status == "student" || status == "teacher" || status == "enrollee" {
		if _, err := connToDatabase.Exec(context.Background(), ""); err != nil {
			return utility.ErrorCreate("SET USER STATUS", err.Error())
		}
	}
	return nil
}
