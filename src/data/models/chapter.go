package models

//ModelChapter - модель глава
type ModelChapter struct {
	ID          int    `json:"id"`
	Number      int    `json:"number"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

//ModelListChapter - список глав
type ModelListChapter struct {
	List []ModelChapter `json:"list"`
}
