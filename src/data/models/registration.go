package models

//ModelRegUser модуль пользователя для регистрации в БД, допускаются пустые поля middlename и emailaddress
type ModelRegUser struct {
	Firstname   string `json:"firstname"`
	Lastname    string `json:"lastname"`
	Middlename  string `json:"middlename"`
	Birthday    string `json:"birthday"`
	Phone       string `json:"phone"`
	Emailaddres string `json:"emailaddres"`
	Pwd         string `json:"pwd"`
}
