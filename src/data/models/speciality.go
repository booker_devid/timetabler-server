package models

//ModelSpeciality - описание сущности специальность
type ModelSpeciality struct {
	Code        string `json:"code"`
	Type        string `json:"type"`
	Description string `json:"description"`
}
