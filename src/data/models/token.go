package models

//TokenModel - Модель токена для авторизации пользователя mode: "student", "master", "admin", "reg"
type TokenModel struct {
	Token string `json:"token"`
	Mode  string `json:"mode"`
}
