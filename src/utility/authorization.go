package utility

import (
	"encoding/base64"
	"fmt"
	"strings"
	"unicode/utf8"

	"github.com/valyala/fasthttp"
)

func stringToAsciiBytes(s string) []byte {
	t := make([]byte, utf8.RuneCountInString(s))
	i := 0
	for _, r := range s {
		t[i] = byte(r)
		i++
	}
	return t
}

//adaptivePhone - функция для адаптации номера телефона:
//+7(123)456-78-90
//+7 (123) 456-78-90
//+7 123 456 78 90
//В итоговую форму: 1234567890
func AdaptivePhone(phone string) string {
	phone = strings.ReplaceAll(phone, "+", "")
	phone = strings.ReplaceAll(phone, "(", "")
	phone = strings.ReplaceAll(phone, ")", "")
	phone = strings.ReplaceAll(phone, "-", "")
	phone = strings.ReplaceAll(phone, " ", "")
	if len(phone) == 11 {
		tmpstring := []rune(phone)
		phone = string(tmpstring[1:])
	}
	phone = string(stringToAsciiBytes(phone))
	return phone

}

//Authorization - fuction for read header
func Authorization(ctx *fasthttp.RequestCtx) (username, password string, ok bool) {
	fmt.Println(ctx.Request.Header.ContentLength())
	auth := ctx.Request.Header.Peek("Authorization")
	if auth != nil {
		return parseBasic(string(auth))
	}
	auth = ctx.Request.Header.Peek("authorization")
	if auth != nil {
		return parseBasic(string(auth))
	}
	return
}

//parseBasic - fuction for parse login and password from header
func parseBasic(auth string) (username, password string, ok bool) {
	const prefix = "Basic "
	if !strings.HasPrefix(auth, prefix) {
		return
	}
	c, err := base64.StdEncoding.DecodeString(auth[len(prefix):])
	if err != nil {
		return
	}
	cs := string(c)
	s := strings.IndexByte(cs, ':')
	if s < 0 {
		return
	}
	return cs[:s], cs[s+1:], true
}
