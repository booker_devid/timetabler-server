package utility

import (
	"encoding/json"
	"io/ioutil"
)

var errorLoad = "Configuration loader module error#"

//Configuration struct needed for mapping config file in object.
type Configuration struct {
	ConnectStrPg string `json:"connect-postgres-database"`
	AddressServ  string `json:"address-server"`
}

//IsValid checking fields for emptitie
func (obj *Configuration) IsValid() bool {
	if obj != nil {
		return (obj.AddressServ != "" && obj.ConnectStrPg != "")
	}
	return false
}

//LoadFile function is reading config file
func LoadFile(path string) (*Configuration, error) {
	fileBytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, ErrorHandler(errorLoad, err)
	}
	return unmarshalToObjConf(fileBytes)
}

//UnmarshalToObjConf function is converting strings in string array
func unmarshalToObjConf(confBytes []byte) (*Configuration, error) {
	var config Configuration
	if err := json.Unmarshal(confBytes, &config); err != nil {
		return nil, ErrorHandler(errorLoad, err)
	}
	return &config, nil
}
