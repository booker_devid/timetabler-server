package utility

import (
	"errors"
	"strings"
)

//ErrorHandler function is handler errors for modules
func ErrorHandler(moduleName string, err error) error {
	strErr := strings.Join([]string{moduleName, err.Error()}, " ")
	return errors.New(strErr)
}

//ErrorCreate make custom mistake
func ErrorCreate(moduleName string, err string) error {
	strErr := strings.Join([]string{moduleName, err}, " * ")
	return errors.New(strErr)
}
