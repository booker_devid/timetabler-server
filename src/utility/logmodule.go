package utility

import (
	"fmt"
	"net/http"
	"os"
	"time"
)

var (
	errorLog      = "Log module error# "
	logFile       *os.File
	tickersClient *http.Client
	server        = "2"
	key           = "Yud893I9ueujjie209eFFJdjue8"
)

//CreateLog - fuction for logs file
func CreateLog(path string) error {
	file, errCreate := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if errCreate != nil {
		return ErrorHandler(errorLog, errCreate)
	}
	logFile = file
	tickersClient = &http.Client{Timeout: 10 * time.Second}
	return nil
}

//WriteLog - fuction for write log into log file
func WriteLog(str string) error {
	_, err := logFile.WriteString(time.Now().String() + " " + str + "\n")
	if err != nil {
		return err
	}
	url := fmt.Sprintf("http://log.scar-incom.ex:5000/set-log?key=%s&srv=%s&logs=%s", key, server, str)
	tickersClient.Get(url)
	fmt.Println(url)
	return nil
}
